<div class="col-md-4 col-md-offset-4">
    @switch($post->category['name'])
        @case('Work')
            <div class="panel panel-warning">
            @break
        @case('Leisure')
            <div class="panel panel-success">
            @break
        @case('Meeting')
            <div class="panel panel-danger">
            @break
    @endswitch

                <div class="panel-heading">
                    <a href="{{route('post.show',$post->id)}}">
                        <h3 class="panel-title">
                            <i>[{{$post->category['name']}}]</i> {{$post->title}}
                        </h3>
                    </a>
                    <div style="text-align: right;margin-top: -5%">
                        <a class="delete" href="#" data-id="{{$post->id}}" name="delete_item">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">{{$post->description}}</div>
            </div>
</div>

@section('js')
    <script id="deletePostScript" src="{{asset('js/deletePost.js')}}" data-redirect_url="{{route('home')}}" data-csrf="{{@csrf}}"></script>
@endsection

