@extends('layouts.master')

@section('title', 'Create Post')


@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="{{route('post.post.create')}}" method="POST">
                <div class="form-group">
                    <label for="title">Title:*</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}" placeholder="eg. Meeting with..">
                </div>
                <div class="form-group">
                    <label for="category">Category:*</label>
                    <select class="form-control" id="category" name="category">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="description">Description:*</label>
                    <textarea class="form-control" id="description" rows="2" name="description">{{old('description')}}</textarea>
                </div>
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                <button type="submit" class="btn btn-success">Create post</button>
                <a href="{{route('home')}}" class="btn btn-danger">Back</a>
                @foreach ($errors->all() as $error)
                    <div style="color: red">{{ $error }}</div>
                @endforeach
            </form>
        </div>
    </div>
@endsection
