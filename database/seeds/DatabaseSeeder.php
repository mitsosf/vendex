<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('categories')->insert([
            'name' => 'Work',
            'created_at' => Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Leisure',
            'created_at' => Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Meeting',
            'created_at' => Carbon::now()
        ]);

        $this->command->info('Seeded the categories');
    }
}
