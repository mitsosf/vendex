$(".delete").on("click", function () {
    let $element = $(this);
    console.log("Id: " + $element.attr("data-id"));

    let post_id = $element.attr("data-id");
    let redirect_url = document.getElementById('deletePostScript').getAttribute('data-redirect_url');
    let csrf = document.getElementById('deletePostScript').getAttribute('data-csrf');

    $.ajax({
        type: "DELETE",
        url: 'post/' + post_id,
        headers: {
            'Content-Type': 'application/json,charset=utf-8',
            'X-CSRF-TOKEN': csrf
        },
        data: {
            _token: csrf
        },
        success: function () {
            window.location = redirect_url;

        },
        error: function () {
            //There is a bug, probably with my Apache config, that while the request succeeds, I get an error thrown
            window.location = redirect_url;
        }
    });
});