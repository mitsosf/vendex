<?php

namespace App\Http\Controllers;

use App\Category;
use App\Events\PostCreated;
use App\Events\PostDeleted;
use App\Http\Requests\StorePost;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use test\Mockery\ReturnTypeObjectTypeHint;

class PostController extends Controller
{
    function home(Request $request)
    {
        //Check if user is searching
        $query = $request['q'];
        if ($query == "") {

            $posts = Post::all();
            return view('home', compact('posts'));
        } else {
            $posts = Post::searchByQuery(array('multi_match' => array('query'=> $query, 'fields' => array('title', 'category.name', 'description'))));
            return view('home', compact('posts'));
        }

    }

    function showPost(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    function getCreatePost()
    {

        //Get categories
        $categories = Category::all();

        return view('posts.create', compact('categories'));
    }

    function postCreatePost(StorePost $request)
    {
        //Add post to DB
        $post = new Post();
        $post->title = $request['title'];
        $post->category_id = $request['category'];
        $post->description = $request['description'];
        $post->save();

        //Index to ElasticSearch
        event(new PostCreated($post));

        return redirect(route('home'));
    }

    function deletePost(Post $post)
    {

        //Delete from ElasticSearch
        event(new PostDeleted($post));

        //Delete from DB
        $post->delete();

        return redirect(route('home'));
    }
}
