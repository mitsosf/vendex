@extends('layouts.master')

@section('title', 'Home')


@section('content')
    <div id="elastic_search">
        <form action="{{route('home')}}">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <input type="text" class="form-control" id="q" name="q" placeholder="Search for post">
                    </div>
                </div>
                <div class="col-md-2">
                    @csrf
                    <button type="submit" class="btn btn-success">Search</button>
                </div>
            </div>
        </form>
    </div>
    @foreach($posts as $post)
        <div class="row" id="posts-container">
            @include('partials.post')
        </div>
    @endforeach
@endsection
