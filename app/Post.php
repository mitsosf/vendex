<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Elasticquent\ElasticquentTrait;

/**
 * @property mixed title
 * @property mixed category_id
 * @property mixed description
 */
class Post extends Model
{
    use ElasticquentTrait;

    protected $fillable = [
        'title', 'category_id', 'description',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
