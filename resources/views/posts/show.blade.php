@extends('layouts.master')

@section('title', 'Show Post')

@section('content')
    <div class="row" id="posts-container">
        @include('partials.post')
    </div>
    <div style="text-align:center">
        <a href="{{route('home')}}" class="btn btn-danger">Back</a>
    </div>
@endsection