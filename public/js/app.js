$(".delete").on("click", function () {
    let $element = $(this);
    console.log("Id: " + $element.attr("data-id"));

    let post_id = $element.attr("data-id");

    $.ajax({
        type: "DELETE",
        url: 'post/' + post_id,
        headers: {
            'Content-Type': 'application/json,charset=utf-8',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            _token: '{{csrf_token()}}'
        },
        success: function () {
            window.location = '{{route('home')}}';

        },
        error: function () {
            window.location = '{{route('home')}}';
        }
    });
});