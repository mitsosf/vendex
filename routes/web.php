<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//General
Route::get('/', 'PostController@home')->name('home');

/*Posts*/

//Show all and search
Route::get('/post/{post}', 'PostController@showPost')->name('post.show');

//Create posts
Route::get('/createPost', 'PostController@getCreatePost')->name('get.post.create');
Route::post('/createPost', 'PostController@postCreatePost')->name('post.post.create');

//Delete post
Route::delete('/post/{post}','PostController@deletePost')->name('delete.post');

