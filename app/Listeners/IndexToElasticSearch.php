<?php

namespace App\Listeners;

use App\Events\PostCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class IndexToElasticSearch
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostCreated $event
     * @return void
     * @throws \Exception
     */
    public function handle(PostCreated $event)
    {
        $post = $event->post;

        $post->category;
        $post->addToIndex();
    }
}
